
public class Circle extends Point{

	private double radius;

	public Circle(int x, int y, double radius) {
		super(x, y);
		this.radius = radius;
	}

    public Circle() {
        super();
    }

    public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public static double area (double radius){
	    return Math.PI * radius*radius;
    }   //static method for any circle

    public boolean isSmalerThen (Circle c2){
        return this.radius < c2.radius ? true : false;
    }
		
}
