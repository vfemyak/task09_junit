import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<Circle> c = new ArrayList<>();
		c.add(new Circle(0, 1, 20));
		c.add(new Circle(1, 5, 8));
		c.add(new Circle(4, 8, 15));
		c.add(new Circle(1, 1, 10));

		for (Circle circle : c) {
			System.out.println("Before : (" + (circle).getA() + ", " + circle.getB() + ")");
			circle.move(Direction.RIGHT, 10);
			circle.move(Direction.UP, 10);
			System.out.println("After : (" + circle.getA() + ", " + circle.getB() + ")");
		}

		c.get(c.size()-1).setRadius(10);

		Circle min = c.get(0);
		for (Circle circle : c) {
			if (circle.getRadius() <= min.getRadius())
				min = circle;
		}
		System.out.println("The smallest circle has coordinates: "
				+ "(" + min.getA() + ", " + min.getB() + ")");

		c.get(0).isSmalerThen(c.get(2));
	}

}
