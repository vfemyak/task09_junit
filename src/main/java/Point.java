
public class Point {
	private int x;
	private int y;

	public Point() {

	}

	public int getA() {
		return x;
	}
	public void setA(int a) {
		this.x = a;
	}
	public int getB() {
		return y;
	}
	public void setB(int b) {
		this.y = b;
	}
	
	public Point(int a, int b) {
		super();
		this.x = a;
		this.y = b;
	}
	
	public void move(Direction d, int step) {
		if (step < 0) {
			throw new ArithmeticException();
		}
		switch (d) {
		case UP:
			y+= step;
			break;
		case DOWN:
			y-= step;
			break;
		case LEFT:
			x-= step;
			break;
		case RIGHT:
			x+= step;
			break;
		default:
			break;
		}
		System.out.println("Moved " + d);
	}
		
}
