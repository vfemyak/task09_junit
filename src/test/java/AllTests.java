import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CircleTest.class,
        PointTest.class
})
public class AllTests {
}
