import org.junit.Before;
import org.junit.Test;

import javax.naming.directory.NoSuchAttributeException;

import static org.junit.Assert.*;

public class PointTest {

    static final Point point = new Point(); //??? //static final constant with package-private or protected access level
    protected Direction directionUp;
    protected Direction directionLeft;

    @Before
    public void init(){
        point.setA(10);
        point.setB(10);
        directionLeft = Direction.LEFT;
        directionUp = Direction.UP;
    }

    @Test
    public void move() {    //краще розбити на декілька тестів чи виконати все в одному?
        point.move(directionUp,20);
        assertEquals("Moving up",30,point.getB());
        assertEquals("Moving left",10,point.getA());
        point.move(directionLeft,10);
        assertEquals("Moving up",30,point.getB());
        assertEquals("Moving left",0,point.getA());
    }

    @Test(expected = ArithmeticException.class)
    public void moveNegativeException() {
        point.move(directionUp,-10);
    }


}