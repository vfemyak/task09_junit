import org.junit.Test;

import static org.junit.Assert.*;

public class CircleTest {

    @Test
    public void area() {
        assertEquals("Passed",314.15926,Circle.area(10),0.001);
    }

    @Test
    public void isSmalerThen() {
        Circle circle1 = new Circle(0,0,3);
        Circle circle2 = new Circle(0,0,3.01);
        assertTrue(circle2.getRadius() > circle1.getRadius());
    }
}